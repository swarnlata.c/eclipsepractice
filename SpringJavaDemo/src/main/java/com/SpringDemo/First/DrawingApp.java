package com.SpringDemo.First;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
public class DrawingApp {
	public static void main(String[] args) {

		ApplicationContext springAppContext = new ClassPathXmlApplicationContext("spring.xml");
		Triangle triangle = (Triangle)springAppContext.getBean("triangle");
		triangle.draw();
	}
}


